#!/bin/bash
mkdir -p ./bundle
git clone https://github.com/VundleVim/Vundle.vim.git ./bundle/Vundle.vim
echo | nvim +PluginInstall +qall #Echo is to simulate newline to fix a halting issue
pip3 install neovim
pip install neovim
